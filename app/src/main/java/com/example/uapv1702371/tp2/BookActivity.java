package com.example.uapv1702371.tp2;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {

    final BookDbHelper BH=new BookDbHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        final Intent intent = getIntent();
        final Book B=(Book) getIntent().getParcelableExtra("book");

        final long id=B.getId();
        final EditText nameText = (EditText)findViewById(R.id.nameBook);
        nameText.setText(B.getTitle());


        final EditText editAuthText = (EditText)findViewById(R.id.editAuthors);
        editAuthText.setText(B.getAuthors());
        final EditText editPublText = (EditText)findViewById(R.id.editYear);
        editPublText.setText(B.getYear());
        final EditText nameeditGenreText = (EditText)findViewById(R.id.editGenres);
        nameeditGenreText.setText(B.getGenres());
        final EditText editEditText = (EditText)findViewById(R.id.editPublisher);
        editEditText.setText(B.getPublisher());


        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nameText.getText().toString().length()>0)
                {



                    final Cursor cur=BH.fetchAllBooks();
                    cur.moveToFirst();
                    boolean j =false;
                    for(int i=0;i<cur.getCount();i++)
                    {
                        Log.v("test",cur.getString(1)+cur.getString(2));
                        if((cur.getString(1).equals(nameText.getText().toString()))&&(cur.getString(2).equals(editAuthText.getText().toString())))
                            j=true;
                        else
                            cur.moveToNext();
                    }

                    if((j)&&(B.getTitle().length()==0))
                    {
                        Toast.makeText(getApplicationContext(), "Un livre d'un même titre d'un même auteur existe déjà",
                                Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Book Bo= new Book(id,nameText.getText().toString(),editAuthText.getText().toString(),editPublText.getText().toString(),nameeditGenreText.getText().toString(),editEditText.getText().toString());
                        //Log.e("ff",(Bo != null)+"");
                        intent.putExtra("book2", Bo );
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Titre vide",
                            Toast.LENGTH_LONG).show();
                }
            }
        });




    }
}
