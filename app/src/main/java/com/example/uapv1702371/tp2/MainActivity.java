package com.example.uapv1702371.tp2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    final BookDbHelper BH=new BookDbHelper(this);
    ArrayAdapter<String> adapter;
    String[] NamaeWa;
    ListView listview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Context context=this;
        final Cursor cur=BH.fetchAllBooks();
        NamaeWa = new String[cur.getCount()];
        for(int i=0;i<cur.getCount();i++)
        {
            NamaeWa[i]=cur.getString(1);
            cur.moveToNext();
        }


        listview  = (ListView) findViewById(R.id.lstView);
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,NamaeWa);
        listview.setAdapter(adapter);

        //Log.e("ff",cur.getCount()+"");

       // final SimpleCursorAdapter ScA=new SimpleCursorAdapter(this,android.R.layout.simple_list_item_1,cur,new String[] { "id" , "title" , "authors","year","genres","publisher"  }, new int[] { R.id.lstView });
       // listview.setAdapter(ScA);

        final Intent intent = new Intent(this, BookActivity.class);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                final String item = (String) parent.getItemAtPosition(position);
                Book b=null;
                cur.moveToFirst();
                for(int i=0;i<cur.getCount();i++)
                {
                    if(item.equals(cur.getString(1)))
                    {
                        b=new Book(Long.parseLong(cur.getString(0)),cur.getString(1),cur.getString(2),cur.getString(3),cur.getString(4),cur.getString(5));
                }
                    cur.moveToNext();
                }
                intent.putExtra("book", b );
                startActivityForResult(intent,1);

            }
        });
        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView parent, View view, final int position, long id) {
                final Cursor cur=BH.fetchAllBooks();
                final String item = (String) parent.getItemAtPosition(position);
                while(!item.equals(cur.getString(1))){
                    cur.moveToNext();
                }
                cur.moveToFirst();
                for(int i=0;i<cur.getCount();i++)
                {
                    if(item.equals(cur.getString(1)))
                    {
                        i=cur.getCount();
                    }
                    else
                    {
                        cur.moveToNext();
                    }
                }
              //  Log.v("Oblivion","pos: " + position);
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                        builder.setTitle("Suppression de livre")
                        .setMessage("Etes-vous sur de vouloir supprimer " + cur.getString(1) + "?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                                BH.deleteBook(cur);
                                Toast.makeText(getApplicationContext(), "Suppression de " + cur.getString(1),
                                        Toast.LENGTH_LONG).show();
                                /*finish();
                                startActivity(getIntent());*/
                                reload();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();

                return true;
            }
        });
        final Intent intent2 = new Intent(this, BookActivity.class);
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingActionButton2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book b=new Book("","","","","");
                intent2.putExtra("book", b );
                startActivityForResult(intent2,2);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == 1)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                final Book B=(Book) data.getParcelableExtra("book2");
                BH.updateBook(B);
                finish();
                startActivity(getIntent());
            }
        }
        else  if(requestCode == 2)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                final Book B=(Book) data.getParcelableExtra("book2");
                BH.addBook(B);
                finish();
                startActivity(getIntent());

            }
        }

    }
    private void reload()
    {
        final Cursor cur=BH.fetchAllBooks();
        NamaeWa = new String[cur.getCount()];
        for(int i=0;i<cur.getCount();i++)
        {
            NamaeWa[i]=cur.getString(1);
            cur.moveToNext();
        }
        cur.moveToFirst();
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,NamaeWa);
        listview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}