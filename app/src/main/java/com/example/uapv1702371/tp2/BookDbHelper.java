package com.example.uapv1702371.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

	// db.execSQL() with the CREATE TABLE ... command
        db.execSQL("CREATE TABLE library (_id INTEGER PRIMARY KEY AUTOINCREMENT, title STRING,authors STRING,year STRING,genres STRING,publisher STRING);");
        populate();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void ROUGE_OF_LOVE()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS library");
        db.execSQL("CREATE TABLE library (_id INTEGER PRIMARY KEY AUTOINCREMENT, title STRING,authors STRING,year STRING,genres STRING,publisher STRING);");
        populate();
        db.close();
    }

   /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();


        //String[] e= new String[1];
        // Inserting Row
       /* db.rawQuery("SELECT max(id) FROM library;",e);
        long rowID = Long.parseLong(e[0]);*/
        long rowID=0;
	   // call db.insert()
        ContentValues CV=new ContentValues();
        CV.put("title",book.getTitle());
        CV.put("authors",book.getAuthors());
        CV.put("year",book.getYear());
        CV.put("genres",book.getGenres());
        CV.put("publisher",book.getPublisher());
        db.insert("library",null,CV);

        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
	    int res=1;

        // updating row
	    // call db.update()
        ContentValues cv = new ContentValues();
        cv.put("title",book.getTitle());
        cv.put("authors",book.getAuthors());
        cv.put("year",book.getYear());
        cv.put("genres",book.getGenres());
        cv.put("publisher",book.getPublisher());

        db.update("library", cv, "_id="+book.getId(), null);
        return res;
    }


    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();

	Cursor cursor;
	// call db.query()
        cursor=db.query("library",null,null,null,null,null,null);
        
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteBook(Cursor cur) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("library", "_id=" +Long.parseLong(cur.getString(0)), null);
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public static Book cursorToBook(Cursor cursor) {
        Book book = null;
	// build a Book object from cursor
        return book;
    }
}
